package net.epitech.java.td03.model.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;
import com.mysema.query.types.PathMetadata;

import javax.annotation.Generated;

import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;

import com.mysema.query.types.Path;
import com.mysema.query.sql.ColumnMetadata;


/**
 * QSmartCard is a Querydsl query type for SmartCard
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSmartCard extends com.mysema.query.sql.RelationalPathBase<SmartCard> {

    private static final long serialVersionUID = -1291249702;

    public static final QSmartCard SmartCard = new QSmartCard("SmartCard");

    public final StringPath holderName = createString("holderName");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public final com.mysema.query.sql.PrimaryKey<SmartCard> primary = createPrimaryKey(id);

    public final com.mysema.query.sql.ForeignKey<SmartCardType> smartCard1Fk = createForeignKey(type, "id");

    public QSmartCard(String variable) {
        super(SmartCard.class,  forVariable(variable), "null", "SmartCard");
        addMetadata();
    }

    public QSmartCard(Path<? extends SmartCard> path) {
        super(path.getType(), path.getMetadata(), "null", "SmartCard");
        addMetadata();
    }

    public QSmartCard(PathMetadata<?> metadata) {
        super(SmartCard.class,  metadata, "null", "SmartCard");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(holderName, ColumnMetadata.named("holderName").ofType(12).withSize(45).notNull());
        addMetadata(id, ColumnMetadata.named("id").ofType(4).withSize(10).notNull());
        addMetadata(type, ColumnMetadata.named("type").ofType(4).withSize(10).notNull());
    }

}

