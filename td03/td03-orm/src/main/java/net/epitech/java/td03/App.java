package net.epitech.java.td03;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.DbUtils;

import net.epitech.java.td03.dao.AbstractSmartCardDAO;
import net.epitech.java.td03.dao.SmartCardDAO;
import net.epitech.java.td03.dao.SmartCardDAOFactory;
import net.epitech.java.td03.exception.SmartCardException;
import net.epitech.java.td03.exception.UnsupportedCodingYearException;
import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;
import net.epitech.java.td03.model.querydsl.QSmartCard;

import com.mysema.query.sql.Configuration;
import com.mysema.query.sql.MySQLTemplates;
import com.mysema.query.sql.SQLQuery;
import com.mysema.query.sql.SQLTemplates;
import com.mysema.query.types.Projections;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class App {

	public static void main(String[] args) {

		SmartCardDAOFactory factory = new SmartCardDAOFactory("root", "root",
				"localhost", "epitech");
		try {
			SmartCardDAO dao2003 = factory.getDaoByYearOfCoding(2003);
			SmartCardDAO dao2005 = factory.getDaoByYearOfCoding(2005);
			SmartCardDAO dao2011 = factory.getDaoByYearOfCoding(2011);
			SmartCardType type = new SmartCardType();
			type.setId(1);
			
			
			
			AbstractSmartCardDAO.showList(dao2003.getSmartCardByType(type) );
			AbstractSmartCardDAO.showList(dao2005.getSmartCardByType(type) );
			AbstractSmartCardDAO.showList(dao2011.getSmartCardByType(type) );
			
		} catch (UnsupportedCodingYearException | SmartCardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
