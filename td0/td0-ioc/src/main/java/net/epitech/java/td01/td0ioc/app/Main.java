package net.epitech.java.td01.td0ioc.app;

import net.epitech.java.td01.td0ioc.config.FirstYearCourseModuleConfiguration;
import net.epitech.java.td01.td0ioc.config.SecondYearCourseModuleConfiguration;
import net.epitech.java.td01.td0ioc.model.contract.Teachable;
import net.epitech.java.td01.td0ioc.model.inj.JavaCourse;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

/**
 * This class show basic usage of IOC with Guice
 * 
 * @author nicolas
 * 
 */
public class Main {

	static Module firstYear = new FirstYearCourseModuleConfiguration();
	static Module secondYear = new SecondYearCourseModuleConfiguration();

	public static void showCourseContent(Module configuration) {

		// that's the new new!
		Injector inj = Guice
				.createInjector(configuration);

		// get default course (implemented by default with englishCouse) Ioc
		// occures for both returned instance type and teacher name
		Teachable englishCourse = inj.getInstance(Teachable.class);
		System.out.println(englishCourse.toString());

		// get Java Course, IOC only occurs for Teacher name
		Teachable javaCourse = inj.getInstance(JavaCourse.class);
		System.out.println(javaCourse.toString());

	}

	public static void main(String[] args) {
		System.out.println("first year:");
		showCourseContent(firstYear);
		System.out.println("second year");
		showCourseContent(secondYear);
	}

}
